# ディレクトリ取得
CURRENT_DIR=$(abspath .)
PARENT_DIR=$(abspath ..)
BASE_DIR:=$(abspath $(dir $(lastword $(MAKEFILE_LIST))))
OUT_DIR=$(BASE_DIR)/doga/_output

# 実行ファイル
AERENDER:=$(BASE_DIR)/utils/sjis_cmd.sh /c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2015/Support\ Files/aerender.exe
RENDER_OPTS:= -sound ON -OMtemplate LagarithRGBA -mp

# コンポ設定
SCENE_NUM=$(shell basename $(PARENT_DIR))
CUT_NUM=$(shell basename $(CURRENT_DIR))
PROJ_FILE=$(CURRENT_DIR)/$(SCENE_NUM)_$(CUT_NUM)honsatu.aep
COMP_NAME=_output
OUT_FILE=$(OUT_DIR)/$(SCENE_NUM)_$(CUT_NUM).avi

#共通処理
define STANDARD_RENDER
     $(BASE_DIR)/utils/sjis_cmd.sh $(AERENDER) -project "$(PROJ_FILE)" -comp "$(COMP_NAME)" -output "$(OUT_FILE)" $(RENDER_OPTS)
endef

# エンコード関連ユーティリティのディレクトリ
UTIL_DIR:=$(BASE_DIR)/utils/bin
ENC_UTIL_DIR:=$(BASE_DIR)/utils/bin

# ユーザー設定
sinclude $(BASE_DIR)/Makefile.user.inc
